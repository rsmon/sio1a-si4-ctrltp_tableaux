package sio1a.si4.ds2;

import java.util.Random;
import static java.lang.Math.abs;

/**
 *
 * @author rsmon
 */

public class SIO1ASI4DS2_E2 {

    static float []    mesurePieces;
    
    static String[]    libelleQualite  = { "EXTRA", "STANDARD", "REBUT"};
    static int[]       effectifQualite = new int[3];
    
    static float       coteNominale=123.5f; 
       
    public static void main(String[] args) {

        acquisitionMesures();
  
        afficherMesuresPieces();       
  
        afficherPiecesExtras();
 
        afficherStatsQualite();
             
    } 
        
    static void afficherMesuresPieces(){
        
      //<editor-fold defaultstate="collapsed" desc="Q1: code Java pour afficher les côtes de toutes les pièces">
      
        
      //</editor-fold>
    
    }
    
    static void afficherPiecesExtras(){
    
      //<editor-fold defaultstate="collapsed" desc="Q2: code Java pour afficher les numéros et côtes des pièces de qualité EXTRA">
       
        //</editor-fold>
    }
    
    static void afficherStatsQualite(){
     
      //<editor-fold defaultstate="collapsed" desc="Q3: code Java pour afficher la statistique">
        
        System.out.println("Exemple d'utilisation de la fonction abs ");
        System.out.println(abs(3-10)); 
        
      //</editor-fold>
    }
    
    // Ne pas modifier cette le code qui suit
    static void acquisitionMesures(){

      Random hasard= new Random();
      int lg=hasard.nextInt(250);
      mesurePieces=new float[lg];
      
      for( int p=1;p<lg;p++){
      
         mesurePieces[p]=hasard.nextFloat()*0.36f+123.5f;
      }
      
    }
}


