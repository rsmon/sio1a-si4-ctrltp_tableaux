package sio1a.si4.ds2;

/**
 *
 * @author rsmon
 */

public class SIO1ASI4DS2_E1_Q4 {
    
    static int[]   temperature; 
    static String  nomDuMois;
    static int     annee;
    
    public static void main(String[] args) {
        
         inititaliserLesDonnees();
        
         //<editor-fold defaultstate="collapsed" desc="code java permettant de calculer et d'afficher la température moyenne du mois.">
        
       
        //</editor-fold>    
        
    }
    
    // Ne pas modifier cette le code qui suit
    static void inititaliserLesDonnees(){
        
        // 
        annee=2014;nomDuMois="Janvier";
        
        // Température relevées au mois de Janvier 2014
        int[] valTemperature={3, 4, 7, 5, -2, -3, 0, -4, -8, -8, -6, -3, 0, 4, 3,
                              2, 5, 4, 5, 6, 6, 5, 6, 6, 8, 10, 12, 10, 9, 10, 8};
        
        temperature=new int[valTemperature.length];
        System.arraycopy(valTemperature, 0, temperature, 0, valTemperature.length);
        
    } 
}


